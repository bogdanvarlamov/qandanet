﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.IdentityModel.Claims;
using System.Threading;
using System.Text;

namespace MvcWebRole1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            StringBuilder sb = new StringBuilder();
            ClaimsIdentity ci = Thread.CurrentPrincipal.Identity as ClaimsIdentity; 
            
            foreach (Claim c in ci.Claims)
            {
                sb.AppendLine("Type: " + c.ClaimType + "- Value: " + c.Value + "");
            }

            ViewBag.Message = sb.ToString();

            return View();
        }
    }
}

﻿using qandaNET.BLL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using qandaNET.Data;
using System.Collections.Generic;
using System.Linq;
using qandaNET.CrossCutting;

namespace Tests
{
    
    
    /// <summary>
    ///This is a test class for PostLogicTest and is intended
    ///to contain all PostLogicTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PostLogicTest
    {
        private const bool _isMock = true;
        private ContentModelContainerMock _context;

        #region Additional test attributes

        /// <summary>
        /// need to build up a clean data context prior to each test
        /// </summary>
        [TestInitialize()]
        public void TestInitialize()
        {
            _context = DataContextFactory.CreateContentContext(_isMock) as ContentModelContainerMock;

            #region User Profiles

            //let's create some user profiles
            var firstUser = new UserProfile() { Id = Guid.NewGuid(), DisplayName = "Test User", Reputation = 50 };
            var secondUser = new UserProfile() { Id = Guid.NewGuid(), DisplayName = "Test User2", Reputation = 12345 };
            _context.UserProfiles.AddObject(firstUser);
            _context.UserProfiles.AddObject(secondUser);

            #endregion //User Profiles


            #region Posts

            //now let's create some posts by the user profiles
            var firstPost = new Post()
            {
                Id = Guid.NewGuid(),
                Author = firstUser,
                Content = "test content in the first post",
                Heading = "Why is this the first question?",
                CreateDate = DateTime.Now,
                Type = (short)PostTypes.Question,
                SeoUrlSuffix = "why-is-this-the-first-question"
            };

            var firstAnswer = new Post()
            {
                Id = Guid.NewGuid(),
                Author = secondUser,
                Content = "test content of the first answer to the first question",
                Heading = "First!",
                CreateDate = DateTime.Now,
                Type = (short)PostTypes.Answer,
                SeoUrlSuffix = "first-answer",
                Parent = firstPost,
            };

            _context.Posts.AddObject(firstPost);
            _context.Posts.AddObject(firstAnswer);

            #endregion //Posts
            
        }

        /// <summary>
        /// clears out the changed DB after each test
        /// so changes aren't persisted between tests
        /// </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            //NOTE: this is messy, there are some extension methods to do this online
            //maybe something like:

            //public static void DeleleAllObjects<TEntity>( this ObjectSet<TEntity> objectSet,
            //                                              IEnumerable<TEntity> objects)
            //    where TEntity : class
            //{

            //   foreach(var o in objects)
            //   {
            //      objectSet.DeleteObject(o);
            //   }
            //}
            //not sure if I want to start extending EF, looks like this will be coming in EF6 anyway
            //maybe think about looping through the properties of the context that are object sets
            //with reflection and deleting them that way?
            foreach(var toDelete in _context.Posts.ToList())
            {
                _context.Posts.DeleteObject(toDelete);
            }

            foreach (var toDelete in _context.UserProfiles.ToList())
            {
                _context.UserProfiles.DeleteObject(toDelete);
            }
        }
        
        #endregion


        #region Test Methods

        #region Multi-Post Operations

        /// <summary>
        ///are posts returned by a post type?
        ///</summary>
        [TestMethod()]
        public void CanGetPostsByType()
        {
            PostLogic target = new PostLogic(_isMock);
            PostTypes pt = PostTypes.Question;
            IEnumerable<Post> expected = _context.Posts.Where(curPost => curPost.Type == (short)pt).Select(curPost => curPost).AsEnumerable();
            IEnumerable<Post> actual = target.GetPostsByType(pt);

            bool allMatch = false;

            if (expected.Count() == actual.Count())
            {
                for (var index = 0; index < expected.Count(); index++)
                {
                    if (expected.ElementAt(index) == actual.ElementAt(index))
                    {
                        //so far so good
                        allMatch = true;
                    }
                    else
                    {
                        allMatch = false; //make sure we know it failed
                        break; //stop looping
                    }
                }
            }
            else
            {
                //different lengths, can't match
            }
            Assert.IsTrue(allMatch);
        }

        /// <summary>
        /// are highest rated questions returning?
        /// </summary>
        [TestMethod()]
        public void CanGetHighestRatedQuestions()
        {
            Assert.Inconclusive();
        }

        /// <summary>
        /// are latest unanswered questions returning?
        /// </summary>
        [TestMethod()]
        public void CanGetLatestUnansweredQuestions()
        {
            Assert.Inconclusive();
        }

        [TestMethod()]
        public void CanGetHighestRatedUnansweredQuestions()
        {
            Assert.Inconclusive();
        }

        #endregion //Multi-Post Operations

        #region Individual Post Operations

        /// <summary>
        /// is a post returned based on it's unique id?
        /// </summary>
        [TestMethod()]
        public void CanFindPostById()
        {
            Assert.Inconclusive();
        }

        /// <summary>
        /// are comments working on answer posts?
        /// </summary>
        [TestMethod()]
        public void CanAddCommentsToAnswerPost()
        {
            Assert.Inconclusive();
        }

        /// <summary>
        /// are answer posts working on question posts?
        /// </summary>
        [TestMethod()]
        public void CanAddAnswersToQuestionPost()
        {
            Assert.Inconclusive();
        }

        /// <summary>
        /// are answers not allowed for closed questions?
        /// </summary>
        [TestMethod()]
        public void NotAllowedToAnswerClosedQuestions()
        {
            Assert.Inconclusive();
        }

        /// <summary>
        /// are comments not allowed for answers where the root question has been closed?
        /// </summary>
        [TestMethod()]
        public void NotAllowedToCommentOnClosedAnswer()
        {
            Assert.Inconclusive();
        }

        /// <summary>
        /// are votes for posts working?
        /// </summary>
        [TestMethod()]
        public void CanVoteOnPost()
        {
            Assert.Inconclusive();
        }

        /// <summary>
        /// is selecting a favorite child post working (e.g. for selecting the best answer to a question)
        /// </summary>
        [TestMethod()]
        public void CanSpecifyFavoriteChildPost()
        {
            Assert.Inconclusive();
        }

        /// <summary>
        /// are duplicate posts tracking properly?
        /// </summary>
        [TestMethod()]
        public void CanSpecifyDuplicateForPost()
        {
            Assert.Inconclusive();

            //TODO: need more thought on this
            // maybe add a "Confirmed Duplicates" type of entity
            // that will allow the original poster to confirm that their question is a duplicate of another
            // and then that will also close question and link back to the younger question
            // also need to add some consideration regarding scenarios... is "creation date" the best way?
            // should popularity and answers/etc. play a role in determining which is considered the duplicate?
            //should both be linked to each other? (what about 10 questions each linking to 10 other duplicates?--crazy?)
            // "DuplicateOf" property probably makes the most sense
        }

        #endregion //Individual Post Operations

        #region Helper Operations

        /// <summary>
        /// is the generated SEO url too long?
        /// </summary>
        [TestMethod()]
        public void GeneratedSeoUrlNotTooLong()
        {
            Assert.Inconclusive();

        }

        /// <summary>
        /// makes sure that the generated SEO url doesn't have filler words like "the" and "in" and etc.
        /// </summary>
        [TestMethod()]
        public void GeneratedSeoUrlLacksFillerWords()
        {
            Assert.Inconclusive();
        }

        #endregion //Helper Operations

        #endregion //Test Methods
    }
}

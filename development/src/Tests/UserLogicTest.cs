﻿using qandaNET.BLL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Tests
{
    
    
    /// <summary>
    ///This is a test class for UserLogicTest and is intended
    ///to contain all UserLogicTest Unit Tests
    ///</summary>
    [TestClass()]
    public class UserLogicTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        /// are we able to get the user profile from the identity?
        /// </summary>
        [TestMethod()]
        public void CanGetProfileFromIdentity()
        {
            Assert.Inconclusive();
        }

        /// <summary>
        /// are we able to create a new user profile?
        /// </summary>
        [TestMethod()]
        public void CanCreateNewProfile()
        {
            Assert.Inconclusive();
        }

        /// <summary>
        /// are we able to associate an identity with an already existing profile?
        /// </summary>
        [TestMethod()]
        public void CanAddIdentityToExistingProfile()
        {
            Assert.Inconclusive();
        }
    }
}

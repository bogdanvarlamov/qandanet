﻿using System;

namespace qandaNET.CrossCutting
{

    public enum PostTypes: short
    {
        Question = 1,
        Answer = 2,
        Comment = 3,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace qandaNET.Data
{
    public static class DataContextFactory
    {
        private static IContentModelContainer _mockContext = new ContentModelContainerMock();

        public static IContentModelContainer CreateContentContext(bool isMockContext)
        {
            if (isMockContext)
                return _mockContext;
            else
                return new ContentModelContainer();
        }
    }
}

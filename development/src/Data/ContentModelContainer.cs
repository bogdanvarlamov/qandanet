//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
// Architectural overview and usage guide: 
// http://blogofrab.blogspot.com/2010/08/maintenance-free-mocking-for-unit.html
//------------------------------------------------------------------------------
using System.Data.EntityClient;
using System.Data.Objects;

namespace qandaNET.Data
{
    /// <summary>
    /// The functional concrete object context. This is just like the normal
    /// context that would be generated using the POCO artefact generator, 
    /// apart from the fact that this one implements an interface containing 
    /// the entity set properties and exposes <code>IObjectSet</code>
    /// instances for entity set properties.
    /// </summary>
    public partial class ContentModelContainer : ObjectContext, IContentModelContainer 
    {
        public const string ConnectionString = "name=ContentModelContainer";
        public const string ContainerName = "ContentModelContainer";
    
        #region Constructors
    
        public ContentModelContainer():
            base(ConnectionString, ContainerName)
        {
            this.ContextOptions.LazyLoadingEnabled = true;
        }
    
        public ContentModelContainer(string connectionString):
            base(connectionString, ContainerName)
        {
            this.ContextOptions.LazyLoadingEnabled = true;
        }
    
        public ContentModelContainer(EntityConnection connection):
            base(connection, ContainerName)
        {
            this.ContextOptions.LazyLoadingEnabled = true;
        }
    
        #endregion
    
        #region ObjectSet Properties
    
        public IObjectSet<Post> Posts
        {
            get { return _posts ?? (_posts = CreateObjectSet<Post>("Posts")); }
        }
        private ObjectSet<Post> _posts;
    
        public IObjectSet<UserProfile> UserProfiles
        {
            get { return _userProfiles ?? (_userProfiles = CreateObjectSet<UserProfile>("UserProfiles")); }
        }
        private ObjectSet<UserProfile> _userProfiles;

        #endregion
    }
}

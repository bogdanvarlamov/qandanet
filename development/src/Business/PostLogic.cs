﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using qandaNET.Data;
using qandaNET.CrossCutting;

namespace qandaNET.BLL
{
    public class PostLogic
    {
        private readonly bool _isMock;

        public PostLogic(bool isMock)
        {
            _isMock = isMock;
        }

        #region Public Methods

        public IEnumerable<Post> GetPostsByType(PostTypes type)
        {
            IEnumerable<Post> toReturn = null;

            using (var db = DataContextFactory.CreateContentContext(_isMock))
            {
                var matches = from curPost in db.Posts
                              where curPost.Type == (short)type
                              select curPost;

                toReturn = matches.AsEnumerable();
            }

            return toReturn;
        }

        #endregion //Public Methods
    }
}

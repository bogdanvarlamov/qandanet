﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace qandaNET.BLL
{
    /// <summary>
    /// the main business logic facade that will abstract all of the other components
    /// and pass in false for the mock parameter to the db context factory
    /// </summary>
    public static class Facade
    {
        #region Constructor

        static Facade()
        {
            Posts = new PostLogic(NOT_MOCKING);
        }

        #endregion //Constructor

        #region Private Members

        private const bool NOT_MOCKING = false;

        #endregion //Private Members

        #region Public Members

        public static PostLogic Posts { get; private set; }

        #endregion //Public Members

        #region Public Methods



        #endregion //Public Methods

        #region Private Methods

        #endregion
    }
}
